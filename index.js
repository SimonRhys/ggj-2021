const SearchLightPattern = require("./SearchLightPattern.js").SearchLightPattern;

const express = require('express');
const app = express();
const http = require('http').Server(app);

const io = require('socket.io')(http, 
   {cors: {origin: true, methods: ["GET", "PUT", "POST"], credentials: true}, 
   path: "/GGJ2021/socket.io/"});

var livereload = require('livereload');
const port = process.env.PORT || 3000;

const MAX_TIMEOUT = 10000;
const INITIAL_SEARCH_LIGHT = {x: 32, z: 32, radius: 5, speed: 15};

var SEARCH_LIGHT_POS = {
   x: INITIAL_SEARCH_LIGHT.x,
   z: INITIAL_SEARCH_LIGHT.z,
   radius: INITIAL_SEARCH_LIGHT.radius,
   speed: INITIAL_SEARCH_LIGHT.speed
};

const KICK_RANGE = 1.25;
const KICK_RANGE_SQ = KICK_RANGE * KICK_RANGE;

app.use("/GGJ2021", express.static(__dirname + '/public'));

var lrserver = livereload.createServer({
   port: 35729,
});

lrserver.watch(__dirname + "/public");

var PLAYERS = [];
var ROUND_START = false;
var SEARCH_LIGHT_DELAY = 0;

io.on("connection", (socket) => {

   if (!ROUND_START) ROUND_START = true;

   console.log("User " + socket.id + " connected");

   io.to(socket.id).emit("SetPlayerID", socket.id);

   PLAYERS[PLAYERS.length] = {
      id: socket.id,
      x: 0,
      y: 0,
      z: 0,
      pitch: 0,
      yaw: 0,
      roll: 0,
      timeout:0,
      dead: false
   };

   socket.on("SendCurrentPosition", (position) => {
      for (var i = 0; i < PLAYERS.length; i++) {
         if (PLAYERS[i].id != socket.id) continue;

         PLAYERS[i].x = position.x;
         PLAYERS[i].y = position.y;
         PLAYERS[i].z = position.z;
         PLAYERS[i].pitch = position.pitch;
         PLAYERS[i].yaw = position.yaw;
         PLAYERS[i].roll = position.roll;
         PLAYERS[i].timeout = 0;

         break;
      }
   });

   socket.on("SendPlayerIsKicking", () => {

      var kickersPosition = {x: 0, y: 0, z: 0, yaw: 0};

      for (var i = 0; i < PLAYERS.length; i++) {
         if (PLAYERS[i].id != socket.id) continue;
         
         kickersPosition.x = PLAYERS[i].x;
         kickersPosition.y = PLAYERS[i].y;
         kickersPosition.z = PLAYERS[i].z;
         kickersPosition.yaw = PLAYERS[i].yaw;
         break;
      }

      for (var i = 0; i < PLAYERS.length; i++) {
         if (PLAYERS[i].id == socket.id) continue;
         
         var x2 = (kickersPosition.x - PLAYERS[i].x) * (kickersPosition.x - PLAYERS[i].x);
         var y2 = (kickersPosition.y - PLAYERS[i].y) * (kickersPosition.y - PLAYERS[i].y);
         var z2 = (kickersPosition.z - PLAYERS[i].z) * (kickersPosition.z - PLAYERS[i].z);

         if (x2 + y2 + z2 < KICK_RANGE_SQ)
         {
            io.to(PLAYERS[i].id).emit("PushBack", kickersPosition);
            break;
         }
      } 

   });

   socket.on("disconnect", () => {
      console.log("User " + socket.id + " disconnected");
      RemovePlayer(socket.id);
   });
});


function Update() {
   if (!ROUND_START) return;

   SEARCH_LIGHT_DELAY += 16;
   if (SEARCH_LIGHT_DELAY > 5000) SEARCH_LIGHT_POS = SearchLightPattern.getNextSearchLight(SEARCH_LIGHT_POS, 16 * 0.001);

   io.emit("SetLightPosition", SEARCH_LIGHT_POS);
   io.emit("UpdatePositions", PLAYERS);

   var radiusSq = SEARCH_LIGHT_POS.radius * SEARCH_LIGHT_POS.radius;

   var numDeadPlayers = 0;

   for (var i = PLAYERS.length - 1; i >= 0 ; i--)
   {
      var diff = {
         x: SEARCH_LIGHT_POS.x - PLAYERS[i].x,
         z: SEARCH_LIGHT_POS.z - PLAYERS[i].z
      };

      diff.x = diff.x * diff.x;
      diff.z = diff.z * diff.z;

      if (diff.x + diff.z < radiusSq && !PLAYERS[i].dead) {
         PLAYERS[i].dead = true;
         io.to(PLAYERS[i].id).emit("Killed");
         console.log(PLAYERS[i].id + " was killed.");
      }

      if (PLAYERS[i].dead) numDeadPlayers++; 


      PLAYERS[i].timeout += 16;

      if (PLAYERS[i].timeout >= MAX_TIMEOUT) {
         PLAYERS.splice(i, 1)
      }
   }

   if (ROUND_START && numDeadPlayers == PLAYERS.length) {
      RestartRound();
   } else if (ROUND_START && numDeadPlayers == PLAYERS.length - 1 && PLAYERS.length > 1) {
      for (var i = 0; i < PLAYERS.length; i++) {
         if (PLAYERS[i].dead) continue;

         ROUND_START = false;
         io.emit("UpdatePositions", PLAYERS);
         io.to(PLAYERS[i].id).emit("SetWon");
         setTimeout(RestartRound, 3000);
         break;
      }
   }
}


function RemovePlayer(id) {
   for (var i = 0; i < PLAYERS.length; i++) {
      if (PLAYERS[i].id != id) continue;
      
      PLAYERS.splice(i, 1);
      break;
   }
}


function RestartRound() {
   console.log("All players found, restarting round.");

   SearchLightPattern.Reset();

   SEARCH_LIGHT_DELAY = 0;

   ROUND_START = false;

   SEARCH_LIGHT_POS.x = INITIAL_SEARCH_LIGHT.x;
   SEARCH_LIGHT_POS.y = INITIAL_SEARCH_LIGHT.y;
   SEARCH_LIGHT_POS.z = INITIAL_SEARCH_LIGHT.z;
   SEARCH_LIGHT_POS.radius = INITIAL_SEARCH_LIGHT.radius;
   SEARCH_LIGHT_POS.speed = INITIAL_SEARCH_LIGHT.speed;

   for (var i = 0; i < PLAYERS.length; i++) {
      PLAYERS[i].x = 0;
      PLAYERS[i].y = 0;
      PLAYERS[i].z = 0;
      PLAYERS[i].dead = false;
   }

   if (PLAYERS.length != 0) {
      ROUND_START = true;
   }

   io.emit("Restart");
}


setInterval(Update, 16);

// Open the port for listening
http.listen(port, () => {
   console.log("listening on *:3000");
});