# Global Game Jam 2021 #

### Install Instructions ###

1. Install NodeJS.
2. Open up a command window.
3. Type 'npm install' and hit enter.


### Run Instructions ###

1. Open up a command window.
2. Type 'node ./index.js' and hit enter.
3. Open a browser and navigate to 'localhost:3000'.


### Controls ###

W - Forward
S - Backward
A - Strafe Left
D - Strafe Right

Left Arrow - Rotate Left
Right Arrow - Rotate Right