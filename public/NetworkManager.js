var NetworkManager = (function() {

   var socket = io.connect({path: "/GGJ2021/socket.io"});

   var PLAYER_ID = "";
   var PLAYER_KICKED = false;
   var KICKED_DIRECTION = 0;
   var OTHER_PLAYERS = [];
   var SEARCH_LIGHT_POSITION = {x: 0, y: 0, z: 0, radius: 0};
   var PLAYER_DEAD = false;
   var PLAYER_KNOWS_DEAD = false;
   var PLAYER_WON = false;
   var PLAYER_KNOWS_WON = false;

   socket.on("SetPlayerID", SetPlayerID);
   socket.on("SetLightPosition", SetSearchLightPosition);
   socket.on("UpdatePositions", UpdatePositions);
   socket.on("PushBack", PushBack);
   socket.on("Killed", SetDead);
   socket.on("SetWon", SetWon);
   socket.on("Restart", Restart);
   

   function SetPlayerID(id) {
      PLAYER_ID = id;
   }


   function SetSearchLightPosition(searchLightPosition) {
      SEARCH_LIGHT_POSITION.x = searchLightPosition.x;
      SEARCH_LIGHT_POSITION.y = searchLightPosition.y;
      SEARCH_LIGHT_POSITION.z = searchLightPosition.z;
      SEARCH_LIGHT_POSITION.radius = searchLightPosition.radius;
   }


   function UpdatePositions(positions) {
      OTHER_PLAYERS = [];

      for (var i = 0; i < positions.length; i++) {
         if (positions[i].id == PLAYER_ID || positions[i].dead) continue;

         var index = OTHER_PLAYERS.length;

         OTHER_PLAYERS[index] = {};
         OTHER_PLAYERS[index].x = positions[i].x;
         OTHER_PLAYERS[index].y = positions[i].y;
         OTHER_PLAYERS[index].z = positions[i].z;
         OTHER_PLAYERS[index].pitch = positions[i].pitch;
         OTHER_PLAYERS[index].yaw = positions[i].yaw;
         OTHER_PLAYERS[index].roll = positions[i].roll;
      }

      var numPlayers = OTHER_PLAYERS.length;

      if (!PLAYER_DEAD) numPlayers++;

      document.getElementById("player-count").innerText = "Num. Players Left: " + numPlayers;
   }


   function PushBack(position) {
      PLAYER_KICKED = true;
      KICKED_DIRECTION = position.yaw;
   }


   function SetDead() {
      document.getElementById("lose-text").style.display = "block";
      PLAYER_DEAD = true;
   }


   function SetWon() {
      document.getElementById("win-text").style.display = "block";
      PLAYER_WON = true;
   }


   function Restart() {
      document.getElementById("win-text").style.display = "none";
      document.getElementById("lose-text").style.display = "none";
      PLAYER_DEAD = false;
      PLAYER_KICKED = false;
      PLAYER_KNOWS_DEAD = false;
      PLAYER_WON = false;
      PLAYER_KNOWS_WON = false;
      main.Restart();
   }

   function GetPlayerPositions() {
      return OTHER_PLAYERS;
   }

   function GetPlayerKickStatus() {
      var isKicked = PLAYER_KICKED;
      PLAYER_KICKED = false;

      return {
         isKicked: isKicked,
         direction: KICKED_DIRECTION
      };
   }

   function GetSearchLightPosition() {
      return SEARCH_LIGHT_POSITION;
   }

   function IsPlayerDead() {
      return PLAYER_DEAD;
   }

   function PlayerAcknowledgeDeath() {
      PLAYER_KNOWS_DEAD = true;
   }

   function HasPlayerAcknowledgedDeath() {
      return PLAYER_KNOWS_DEAD;
   }

   function GetPlayerWon() {
      return PLAYER_WON;
   }

   function PlayerAcknowledgeVictory() {
      PLAYER_KNOWS_WON = true;
   }

   function HasPlayerAcknowledgedVictory() {
      return PLAYER_KNOWS_WON;
   }

   function SendCurrentPosition(position) {
      socket.emit("SendCurrentPosition", position);
   }


   function SendPlayerIsKicking() {
      socket.emit("SendPlayerIsKicking");
   }


   return {
      GetPlayerPositions: GetPlayerPositions,
      SendCurrentPosition: SendCurrentPosition,
      SendPlayerIsKicking: SendPlayerIsKicking,
      GetPlayerKickStatus: GetPlayerKickStatus,
      GetSearchLightPosition: GetSearchLightPosition,
      IsPlayerDead: IsPlayerDead,
      PlayerAcknowledgeDeath: PlayerAcknowledgeDeath,
      HasPlayerAcknowledgedDeath: HasPlayerAcknowledgedDeath,
      GetPlayerWon: GetPlayerWon,
      PlayerAcknowledgeVictory: PlayerAcknowledgeVictory,
      HasPlayerAcknowledgedVictory: HasPlayerAcknowledgedVictory
   }

})();