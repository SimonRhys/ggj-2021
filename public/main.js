var main = (function () {

//--------------------------------------------------------------------------
// Global Variables

var gl = null;

var CANVAS;

var PREVIOUS_FRAME = 0;
var SPEED = 5;
var GRAVITY = 10;
var KICK_COOLDOWN = 0;
var PUSH_STRENGTH = 50;
var SPRINT_MODIFIER = 2.5;
var MAX_SPRINT_TIMER = 2;
var SPRINT_TIMER = MAX_SPRINT_TIMER;
var CAN_SPRINT = true;

var PLAYER_SIZE = 1;

var START_POSITION = {x: 4, y: 0, z: 4, pitch: 0, yaw: Math.PI * 0.25, roll: 0};

var CAMERA = {
   proj: new Matrix4(),
   orthoProj: new Matrix4(),
   view: new Matrix4(),
   yaw: Math.PI * 0.75,
   pitch: Math.PI * 0.35,
   xOffset: -3,
   yOffset: 10,
   zOffset: -3,
};

var PLAYER_POSITION = {
   x: START_POSITION.x,
   y: START_POSITION.y,
   z: START_POSITION.z,
   pitch: START_POSITION.pitch,
   yaw: START_POSITION.yaw,
   roll: START_POSITION.roll,
};

var TIMER = 0;

var PLAYER_ACCELERATION = {x: 0, y: 0, z: 0};

{
   var rotate = Matrix4.Rotate(CAMERA.pitch, CAMERA.yaw, 0);
   var translate = Matrix4.Translate(
      -PLAYER_POSITION.x - CAMERA.xOffset,
      -PLAYER_POSITION.y - CAMERA.yOffset,
      -PLAYER_POSITION.z - CAMERA.zOffset);
   
   CAMERA.view = Matrix4.Multiply(translate, rotate);
}

var SUN_DIR = {x: 1, y: 4, z: 1};

var PLAYER_SHADER = new Shader();
var INSTANCED_SHADER = new Shader();
var UI_SHADER = new Shader();

var MODEL_TEXTURES = [];

var CUBE_SIZE = 2;
var TERRAIN = new InstancedModel();
var MAP = [];

var OTHER_PLAYER_MODEL = new Model();
var SPRINT_BAR = new Sprite();

var SEARCHLIGHT = {x: 0, y: 0, z: 0, radius: 0};

var MUSIC_CLOCK = 0;
var EXTRA_SPECIAL_MUSIC_TRACK = false;
var MUTE = false;

//--------------------------------------------------------------------------
// This function is called once the HTML page has finished loading. It sets
// up all the listening events for controls, sets up the WebGL, loads the
// required models and basically anything else needed before starting the
// animation loop.
//
async function Start() {
   CANVAS = document.getElementById("glcanvas", { alpha: false });
   CANVAS.oncontextmenu = function(e) {
      e.preventDefault();
   };
   
   if (!InitWebGL()) return;
   
   gl.clearColor(0.0, 0.0, 0.0, 1.0);
   gl.enable(gl.CULL_FACE);
   gl.enable(gl.DEPTH_TEST);

   // Not sure why this is behaving so badly...
   //
   gl.enable(gl.BLEND);
   gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

   gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
   gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
   
   window.addEventListener("resize", ResizeCanvas);
   
   // CANVAS.requestPointerLock = CANVAS.requestPointerLock ||
   // CANVAS.mozRequestPointerLock;

   // CANVAS.onclick = function() {
   //     CANVAS.requestPointerLock();
   // }

   document.onkeydown = InputManager.HandleKeyDown;
   document.onkeyup = InputManager.HandleKeyUp;
   // document.onmousemove = InputManager.HandleMouseMove;
   // document.addEventListener('pointerlockchange', InputManager.HandlePointerLock, false);
   // document.addEventListener('mozpointerlockchange', InputManager.HandlePointerLock, false);

   InputManager.CustomiseControls(InputManager.Controls.MOVE_FORWARD, "W".charCodeAt(0));
   InputManager.CustomiseControls(InputManager.Controls.MOVE_BACKWARD, "S".charCodeAt(0));
   InputManager.CustomiseControls(InputManager.Controls.MOVE_LEFT, "A".charCodeAt(0));
   InputManager.CustomiseControls(InputManager.Controls.MOVE_RIGHT, "D".charCodeAt(0));
   
   InputManager.CustomiseControls(InputManager.Controls.ROTATE_LEFT, 37);
   InputManager.CustomiseControls(InputManager.Controls.ROTATE_RIGHT, 39);
   
   InputManager.CustomiseControls(InputManager.Controls.INTERACT, "E".charCodeAt(0));
   
   SPRINT_BAR.InitGL(gl);
   
   ResizeCanvas();
   
   PLAYER_SHADER.Init(gl);
   PLAYER_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertexTextured"), gl.VERTEX_SHADER);
   PLAYER_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragmentTextured"), gl.FRAGMENT_SHADER);
   PLAYER_SHADER.CreateProgram();

   INSTANCED_SHADER.Init(gl);
   INSTANCED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertexTexturedInstanced"), gl.VERTEX_SHADER);
   INSTANCED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragmentTexturedInstanced"), gl.FRAGMENT_SHADER);
   INSTANCED_SHADER.CreateProgram();

   UI_SHADER.Init(gl);
   UI_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertexUI"), gl.VERTEX_SHADER);
   UI_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragmentUI"), gl.FRAGMENT_SHADER);
   UI_SHADER.CreateProgram();

   var terrainTexture = new Texture();
   terrainTexture.InitGL(gl);
   terrainTexture.LoadTexture("Textures/floorboard.png");

   TERRAIN.InitGL(gl);
   TERRAIN.SetScale(CUBE_SIZE, CUBE_SIZE, CUBE_SIZE);
   TERRAIN.SetTextureID(terrainTexture.GetID());

   var positions = [];

   for (var x = 0; x < 32; x++) {
      MAP[x] = [];
      for (var z = 0; z < 32; z++) {
         var y = 0;
   
         positions[positions.length] = x * CUBE_SIZE;
         positions[positions.length] = y * CUBE_SIZE;
         positions[positions.length] = z * CUBE_SIZE;
   
         MAP[x][z] = y * CUBE_SIZE;
      }
   }

   TERRAIN.SetPerInstanceData(positions, 3, 3);

   SPRINT_BAR.SetPosition(CANVAS.width * 0.5 - CANVAS.width * 0.1, CANVAS.height * 0.2, 0.0);
   SPRINT_BAR.SetSize(CANVAS.width * 0.2, 16);

   await  SoundManager.Load("Audio/ouch1.wav", "ouch");
   await  SoundManager.Load("Audio/kick4.wav", "kick");
   await  SoundManager.Load("Audio/background1.wav", "background1");
   await  SoundManager.Load("Audio/background2.wav", "background2");
   await  SoundManager.Load("Audio/background3.wav", "background3");
   await  SoundManager.Load("Audio/background4.wav", "background4");
   await  SoundManager.Load("Audio/background5.wav", "background5");
   await  SoundManager.Load("Audio/ded.wav", "ondeath");
   await  SoundManager.Load("Audio/win2.wav", "onwin");
   await  SoundManager.Load("Audio/how are you still alive.wav", "how");

   await LoadAllModels();

   window.requestAnimationFrame(Update);
}


//--------------------------------------------------------------------------
// This function sets up the WebGL context.
//
function InitWebGL() {
   try {
      gl = CANVAS.getContext("webgl2", {alpha: true});
   }
   catch(e) {
   }

   //--------------------------------------------------------------------------
   // If we don't have a GL context, give up now

   if (!gl) {
      alert("Unable to initialize WebGL. Your browser may not support it.");
      return false;
   }

   return true;
}


//--------------------------------------------------------------------------
// This function is the main loop of the game. It will be called every frame.
//
function Update(currentFrame) {

   var dt = Math.min((currentFrame - PREVIOUS_FRAME) * 0.001, 0.032);
   PREVIOUS_FRAME = currentFrame;
   
   TIMER += dt;
   
   //Background music
   MUSIC_CLOCK += dt;
   if (!SoundManager.IsPlaying("background1")) {
      SoundManager.SetVolume("background1", 0.1 * !MUTE);
      SoundManager.Play("background1", true);
   }

   if (!SoundManager.IsPlaying("background2") && MUSIC_CLOCK >= 10) {
      SoundManager.SetVolume("background2", 0.1 * !MUTE);
      SoundManager.Play("background2", true);
   }

   if (!SoundManager.IsPlaying("background3") && MUSIC_CLOCK >= 20) {
      SoundManager.SetVolume("background3", 0.05 * !MUTE);
      SoundManager.Play("background3", true);
   }

   if (!SoundManager.IsPlaying("background4") && MUSIC_CLOCK >= 30) {
      SoundManager.SetVolume("background4", 0.1 * !MUTE);
      SoundManager.Play("background4", true);
   }

   if (!SoundManager.IsPlaying("background5") && MUSIC_CLOCK >= 50) {
      SoundManager.SetVolume("background5", 0.1 * !MUTE);
      SoundManager.Play("background5", true);
   }

   if (KICK_COOLDOWN > 0 && !SoundManager.IsPlaying("kick")) {
      SoundManager.SetVolume("kick", 0.4);
      SoundManager.Play("kick", false);
   }

   if(NetworkManager.IsPlayerDead() && !NetworkManager.HasPlayerAcknowledgedDeath()){
      SoundManager.SetVolume("ondeath", 0.4);
      SoundManager.Play("ondeath", false);
      NetworkManager.PlayerAcknowledgeDeath();
   }

   if(NetworkManager.GetPlayerWon() && !NetworkManager.HasPlayerAcknowledgedVictory()) {
      SoundManager.SetVolume("onwin", 0.4);
      SoundManager.Play("onwin", false);
      NetworkManager.PlayerAcknowledgeVictory();
   }

   if(!EXTRA_SPECIAL_MUSIC_TRACK && MUSIC_CLOCK >= 70){
      SoundManager.SetVolume("how", 0.4);
      SoundManager.Play("how", false);
      EXTRA_SPECIAL_MUSIC_TRACK = true;
   }
   
   KICK_COOLDOWN = Math.max(KICK_COOLDOWN - dt, 0.0);
   PLAYER_ACCELERATION.x = PLAYER_ACCELERATION.x * 0.95;
   PLAYER_ACCELERATION.z = PLAYER_ACCELERATION.z * 0.95;

   if (PLAYER_ACCELERATION.x < 0.001) PLAYER_ACCELERATION.x = 0;
   if (PLAYER_ACCELERATION.z < 0.001) PLAYER_ACCELERATION.z = 0;

   PLAYER_ACCELERATION.y = PLAYER_ACCELERATION.y - GRAVITY * dt;
   HandleControls(dt);

   SEARCHLIGHT = NetworkManager.GetSearchLightPosition();
   NetworkManager.SendCurrentPosition(PLAYER_POSITION);

   if(InputManager.IsKicking() && KICK_COOLDOWN == 0){

      NetworkManager.SendPlayerIsKicking();
      KICK_COOLDOWN = 0.5;
   }

   Draw();

   window.requestAnimationFrame(Update);
}


//--------------------------------------------------------------------------
// This function is responsible for drawing everything.
//
function Draw() {

   //--------------------------------------------------------------------------
   // Clear the canvas before we start drawing on it.

   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


   //--------------------------------------------------------------------------
   // Draw all the instanced models that do not have textures.

   INSTANCED_SHADER.Use();
   INSTANCED_SHADER.UniformMatrix4fv("proj", CAMERA.proj);
   INSTANCED_SHADER.UniformMatrix4fv("view", CAMERA.view);

   INSTANCED_SHADER.Uniform3f("lightDir", SUN_DIR.x, SUN_DIR.y, SUN_DIR.z);
   INSTANCED_SHADER.Uniform3f("searchLightPos", SEARCHLIGHT.x, SEARCHLIGHT.y, SEARCHLIGHT.z);
   INSTANCED_SHADER.Uniform1f("searchLightRadius", SEARCHLIGHT.radius);
   INSTANCED_SHADER.Uniform3f("searchLightColour", 0.9, 0.9, 0.1);

   TERRAIN.Draw(INSTANCED_SHADER);


   //--------------------------------------------------------------------------
   // Draw all the instanced models that do not have textures.

   PLAYER_SHADER.Use();
   PLAYER_SHADER.UniformMatrix4fv("proj", CAMERA.proj);
   PLAYER_SHADER.UniformMatrix4fv("view", CAMERA.view);

   PLAYER_SHADER.Uniform3f("lightDir", SUN_DIR.x, SUN_DIR.y, SUN_DIR.z);
   PLAYER_SHADER.Uniform1f("dead", 0);

   var otherPlayers = NetworkManager.GetPlayerPositions();
   for (var i = 0; i < otherPlayers.length; i++) {
      var x = otherPlayers[i].x;
      var y = otherPlayers[i].y;
      var z = otherPlayers[i].z;
      var pitch = otherPlayers[i].pitch;
      var yaw = otherPlayers[i].yaw;
      var roll = otherPlayers[i].roll;

      OTHER_PLAYER_MODEL.SetPosition(x, y, z, pitch, yaw, roll);
      OTHER_PLAYER_MODEL.Draw(PLAYER_SHADER);
   }

   PLAYER_SHADER.Uniform1f("dead", NetworkManager.IsPlayerDead());

   OTHER_PLAYER_MODEL.SetPosition(
      PLAYER_POSITION.x,
      PLAYER_POSITION.y,
      PLAYER_POSITION.z,
      PLAYER_POSITION.pitch,
      PLAYER_POSITION.yaw,
      PLAYER_POSITION.roll);

   OTHER_PLAYER_MODEL.Draw(PLAYER_SHADER);

   UI_SHADER.Use();
   UI_SHADER.UniformMatrix4fv("proj", CAMERA.orthoProj);
   UI_SHADER.Uniform3f("colour", 0.0, 0.0, 1.0);

   SPRINT_BAR.SetSize(CANVAS.width * 0.2 * (SPRINT_TIMER / MAX_SPRINT_TIMER) , 16);
   SPRINT_BAR.Draw(UI_SHADER);
}


//--------------------------------------------------------------------------
// This function takes in user input that was mapped to the InputManager in
// the Start() function and applies it to the camera. Collision is also
// handled in here.
//
function HandleControls(dt) {
   var move = {x: 0, y: 0, z: 0, sprint: 1,  pitch: 0, yaw: 0, kick: false};
   var didMove = false;

   var kickStatus = NetworkManager.GetPlayerKickStatus();

   if (kickStatus.isKicked){

      PLAYER_ACCELERATION.x = Math.sin(kickStatus.direction) * PUSH_STRENGTH;
      PLAYER_ACCELERATION.y = PUSH_STRENGTH/10;
      PLAYER_ACCELERATION.z = Math.cos(kickStatus.direction) * PUSH_STRENGTH;

      if (!SoundManager.IsPlaying("ouch")) {
         SoundManager.SetVolume("ouch", 0.2);
         SoundManager.Play("ouch", false);
      }
   }
   
   move.x += PLAYER_ACCELERATION.x;
   move.y += PLAYER_ACCELERATION.y;
   move.z += PLAYER_ACCELERATION.z;
   didMove = true;

   if (InputManager.IsKeyPressed(16) && CAN_SPRINT) {
      move.sprint = SPRINT_MODIFIER;
      SPRINT_TIMER -= 2 * dt;

      if (SPRINT_TIMER < 0) CAN_SPRINT = false;
   } else {
      SPRINT_TIMER = Math.min(SPRINT_TIMER + dt * 0.5, MAX_SPRINT_TIMER);

      if (SPRINT_TIMER > MAX_SPRINT_TIMER * 0.1) CAN_SPRINT = true;
   }

   if (InputManager.IsMovingForward()) {
      move.x -= SPEED * Math.cos(CAMERA.yaw + Math.PI * 0.5) * move.sprint;
      move.z -= SPEED * Math.sin(CAMERA.yaw + Math.PI * 0.5) * move.sprint;
      didMove = true;
   }

   if (InputManager.IsMovingBackward()) {
      move.x += SPEED * Math.cos(CAMERA.yaw + Math.PI * 0.5) * move.sprint;
      move.z += SPEED * Math.sin(CAMERA.yaw + Math.PI * 0.5) * move.sprint;
      didMove = true;
   }

   if (InputManager.IsMovingLeft()) {
      move.x -= SPEED * Math.cos(CAMERA.yaw) * move.sprint;
      move.z -= SPEED * Math.sin(CAMERA.yaw) * move.sprint;
      didMove = true;
   }

   if (InputManager.IsMovingRight()) {
      move.x += SPEED * Math.cos(CAMERA.yaw) * move.sprint;
      move.z += SPEED * Math.sin(CAMERA.yaw) * move.sprint;
      didMove = true;
   }

   if (InputManager.IsMovingForward() && InputManager.IsMovingRight()) {
      PLAYER_POSITION.yaw = 0.0;
   } else if (InputManager.IsMovingForward() && InputManager.IsMovingLeft()) {
      PLAYER_POSITION.yaw = Math.PI * 0.5;
   } else if (InputManager.IsMovingBackward() && InputManager.IsMovingRight()) {
      PLAYER_POSITION.yaw = -Math.PI * 0.5;
   } else if (InputManager.IsMovingBackward() && InputManager.IsMovingLeft()) {
      PLAYER_POSITION.yaw = -Math.PI;
   } else if (InputManager.IsMovingForward()) {
      PLAYER_POSITION.yaw = Math.PI * 0.25;
   } else if (InputManager.IsMovingBackward()) {
      PLAYER_POSITION.yaw = Math.PI + Math.PI * 0.25;
   } else if (InputManager.IsMovingRight()) {
      PLAYER_POSITION.yaw = -Math.PI * 0.25;
   } else if (InputManager.IsMovingLeft()) {
      PLAYER_POSITION.yaw = -Math.PI * 0.25 + Math.PI;
   }

   if (move.x != 0 || move.z != 0) PLAYER_POSITION.roll = Math.cos(TIMER * SPEED) * 0.25;
   else PLAYER_POSITION.roll = 0;

   if (KICK_COOLDOWN != 0) {
      PLAYER_POSITION.yaw += Math.sin(4 * Math.PI * KICK_COOLDOWN);
   }

   if (didMove) {
      PLAYER_POSITION.x += move.x * dt;
      PLAYER_POSITION.z += move.z * dt;
      

      PLAYER_POSITION.y = Math.max(PLAYER_POSITION.y + move.y * dt, CUBE_SIZE);
      
      HandleCollision("X", move.x * dt);
      HandleCollision("Z", move.z * dt);

      var x_zoom_dist = SEARCHLIGHT.x - PLAYER_POSITION.x;
      var z_zoom_dist = SEARCHLIGHT.z - PLAYER_POSITION.z;

      var ZOOM_MAX = -40;
      var zoom = Math.sqrt((x_zoom_dist*x_zoom_dist) + (z_zoom_dist*z_zoom_dist));
      var camera_y = Clamp(-PLAYER_POSITION.y - CAMERA.yOffset - zoom, ZOOM_MAX, -30);

      var half_zoom = zoom * 0.333;
      var camera_x = -PLAYER_POSITION.x - CAMERA.xOffset + half_zoom;
      var camera_z = -PLAYER_POSITION.z - CAMERA.zOffset + half_zoom;

      var rotate = Matrix4.Rotate(CAMERA.pitch, CAMERA.yaw, 0);
      var translate = Matrix4.Translate(
         camera_x,
         camera_y,
         camera_z);
      
      CAMERA.view = Matrix4.Multiply(translate, rotate);
   }
}


//--------------------------------------------------------------------------
// This function handles the collision of the player with the terrain.
//
function HandleCollision(axis, movedBy) {
   var xIndex = Clamp(Math.floor(PLAYER_POSITION.x / CUBE_SIZE), 0, MAP.length - 1);
   var zIndex = Clamp(Math.floor(PLAYER_POSITION.z / CUBE_SIZE), 0, MAP.length - 1);

   if (PLAYER_POSITION.x < 0 || 
       PLAYER_POSITION.z < 0 || 
       PLAYER_POSITION.x > MAP.length * CUBE_SIZE - PLAYER_SIZE ||
       PLAYER_POSITION.z > MAP.length * CUBE_SIZE - PLAYER_SIZE) {
      
      if (axis == "X") PLAYER_POSITION.x -= movedBy;
      if (axis == "Z") PLAYER_POSITION.z -= movedBy;
   }

   // if (MAP[xIndex][zIndex] > PLAYER_POSITION.y * CUBE_SIZE - PLAYER_HEIGHT) {
   //    AIM_HEIGHT = MAP[xIndex][zIndex] + PLAYER_HEIGHT;
   //    SMOOTH_Y_TRANSITION = true;
   //    PLAYER_ACCELERATION.y = 0;
   // }
}


//--------------------------------------------------------------------------
// This function loads all the '.obj' models listed in the 'model-list.txt'
// file.
//
async function LoadAllModels() {
   var res = await fetch("Models/model-list.txt");
   var text = await res.text();

   var lines = text.split('\n');

   for (var i = 0; i < lines.length; i++) {
      var model = await LoadModel("Models/", lines[i]);

      if (lines[i].includes("player")) {
         OTHER_PLAYER_MODEL.InitGL(gl);
         OTHER_PLAYER_MODEL.SetVertexData(model.vertexData);
         OTHER_PLAYER_MODEL.SetTextureID(model.textureID);
         OTHER_PLAYER_MODEL.SetScale(0.25, 0.25, 0.25);
      }
   }

   window.requestAnimationFrame(Update);
}


//--------------------------------------------------------------------------
// This function loads a specified '.obj' model and returns the vertex data
// neccessary to create either a Model() or an InstancedModel(). 
// 
// NOTE: I have only tested this with MagicaVoxel (https://ephtracy.github.io/)
//       exported '.obj' files. There are probably bugs if using a generic
//       '.obj' file.
//
async function LoadModel(dir, fileName) {
   var res = await fetch(dir + fileName);
   var text = await res.text();

   var lines = text.split('\n');

   var mtlFileName = "";

   var vertices = [];
   var normals = [];
   var texCoords = [];

   var vertexData = [];

   for (var i = 0; i < lines.length; i++) {
      var l = lines[i].split(' ');
      l[0] = l[0].trim();

      if (l[0] == "mtllib") {
         mtlFileName = l[1];

      } else if (l[0] == "vn") {
         normals[normals.length] = parseFloat(l[1]);
         normals[normals.length] = parseFloat(l[2]);
         normals[normals.length] = parseFloat(l[3]);

      } else if (l[0] == "vt") {
         texCoords[texCoords.length] = parseFloat(l[1]);
         texCoords[texCoords.length] = parseFloat(l[2]);

      } else if (l[0] == "v") {
         vertices[vertices.length] = parseFloat(l[1]);
         vertices[vertices.length] = parseFloat(l[2]);
         vertices[vertices.length] = parseFloat(l[3]);

      } else if (l[0] == "f") {
         for (var j = 1; j < 4; j++) {
            var v = l[j].split('/');

            var vertexIndex = parseInt(v[0]) - 1;
            var textureIndex = parseInt(v[1]) - 1;
            var normalIndex = parseInt(v[2]) - 1;

            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 0];
            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 1];
            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 2];

            vertexData[vertexData.length] = texCoords[textureIndex * 2 + 0];
            vertexData[vertexData.length] = texCoords[textureIndex * 2 + 1];

            vertexData[vertexData.length] = normals[normalIndex * 3 + 0];
            vertexData[vertexData.length] = normals[normalIndex * 3 + 1];
            vertexData[vertexData.length] = normals[normalIndex * 3 + 2];
         }
      }
   }

   var textureID = await LoadMTL(dir, mtlFileName);

   return {vertexData: vertexData, textureID: textureID};
}


//--------------------------------------------------------------------------
// This function loads a specified '.mtl' file associated with a '.obj' file.
// 
// NOTE: I have only tested this with MagicaVoxel (https://ephtracy.github.io/)
//       exported '.obj' files. There are probably bugs if using a generic
//       '.obj' file.
//
async function LoadMTL(dir, filePath) {
   var res = await fetch(dir + filePath);
   var text = await res.text();

   var lines = text.split('\n');

   var texFilePath = "";
   
   for (var i = 0; i < lines.length; i++) {
      var l = lines[i].split(' ');
      l[0] = l[0].trim();

      if (l[0] == "map_Kd") {
         texFilePath = l[1];
      }
   }

   var modelTextureIndex = MODEL_TEXTURES.length;
   MODEL_TEXTURES[modelTextureIndex] = new Texture();
   MODEL_TEXTURES[modelTextureIndex].InitGL(gl);
   await MODEL_TEXTURES[modelTextureIndex].LoadTexture(dir + texFilePath);

   return MODEL_TEXTURES[modelTextureIndex].GetID();
}


//--------------------------------------------------------------------------
// This function resizes the WebGL canvas whenever the browser window is
// also resized.
//
function ResizeCanvas() {
   CANVAS = document.getElementById("glcanvas");
   CANVAS.width = CANVAS.clientWidth;
   CANVAS.height = CANVAS.clientHeight;

   gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

   CAMERA.proj = Matrix4.GetPerspective(Math.PI * 0.25, CANVAS.width / CANVAS.height, 0.1, 10000);
   CAMERA.orthoProj = Matrix4.GetOrthographic(0, CANVAS.width, 0, CANVAS.height, -1.0, 1.0);

   SPRINT_BAR.SetPosition(CANVAS.width * 0.5 - CANVAS.width * 0.1, CANVAS.height * 0.2, 0.0);
   SPRINT_BAR.SetSize(CANVAS.width * 0.2, 16);
}


//--------------------------------------------------------------------------
// This function clamps a value between a minimum value and a maximum.
//
function Clamp(t, min, max) {
   t = Math.max(t, min);
   t = Math.min(t, max);

   return t;
}


function SetPlayerAcceleration(acc) {

}


function Restart() {
   PLAYER_ACCELERATION.x = 0;
   PLAYER_ACCELERATION.y = 0;
   PLAYER_ACCELERATION.z = 0;

   PLAYER_POSITION.x = START_POSITION.x;
   PLAYER_POSITION.y = START_POSITION.y;
   PLAYER_POSITION.z = START_POSITION.z;
   PLAYER_POSITION.pitch = START_POSITION.pitch;
   PLAYER_POSITION.yaw = START_POSITION.yaw;
   PLAYER_POSITION.roll = START_POSITION.roll;

   KICK_COOLDOWN = 0;

   SPRINT_TIMER = MAX_SPRINT_TIMER;

   SoundManager.Reset("background1");
   SoundManager.Reset("background2");
   SoundManager.Reset("background3");
   SoundManager.Reset("background4");
   SoundManager.Reset("background5");

   MUSIC_CLOCK = 0;
   EXTRA_SPECIAL_MUSIC_TRACK = false;
}


return {
   Start: Start,
   SetPlayerAcceleration: SetPlayerAcceleration,
   Restart: Restart,
}

})();