var ShaderSources = (function() {

   var SOURCES = {};

   SOURCES.drawVertexTextured = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;
      layout(location=2) in vec3 normal;

      uniform mat4 proj;
      uniform mat4 view;
      uniform mat4 model;

      out vec3 vPos;
      out vec2 vTexCoords;
      out vec3 vNormal;

      void main(void) 
      {
         vec4 mPos = model * vec4(position, 1.0);
         gl_Position = proj * view * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
         vNormal = mat3(transpose(inverse(model))) * normal;
      }
   `;

   SOURCES.drawFragmentTextured = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;
      in vec3 vNormal;

      uniform sampler2D tex;
      uniform vec3 lightDir;
      uniform float dead;

      out vec4 fragColour;
      void main(void) 
      {  
         vec3 normal = normalize(vNormal);
         vec3 normLightDir = normalize(lightDir);

         float ambient = 0.5;
         float diffuse = max(dot(normal, normLightDir), 0.0);

         vec4 colour = texture(tex, vTexCoords);
         colour.xyz = colour.xyz * ambient;

         fragColour = vec4(colour.rgb, 1.0 - 0.5 * dead);
      }
   `;


   SOURCES.drawVertexTexturedInstanced = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;
      layout(location=2) in vec3 normal;
      layout(location=3) in vec3 offset;

      uniform mat4 proj;
      uniform mat4 view;
      uniform mat4 model;

      out vec3 vPos;
      out vec2 vTexCoords;
      out vec3 vNormal;

      void main(void) 
      {
         vec4 mPos = model * vec4(position, 1.0);
         mPos.xyz += offset;
         
         gl_Position = proj * view * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
         vNormal = mat3(transpose(inverse(model))) * normal;
      }
   `;


   SOURCES.drawFragmentInstanced = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;
      in vec3 vNormal;

      uniform vec3 lightDir;
      uniform vec3 colour;

      uniform vec3 searchLightPos;
      uniform float searchLightRadius;
      uniform vec3 searchLightColour;

      out vec4 fragColour;
      void main(void) 
      {  
         vec3 normal = normalize(vNormal);
         vec3 normLightDir = normalize(lightDir);

         float ambient = 0.5;
         float diffuse = dot(normal, normLightDir);

         fragColour = vec4(colour * (ambient + diffuse), 1.0);

         vec2 distance_vec = vPos.xz - searchLightPos.xz;
         float mag_sqr = pow(distance_vec.x, 2.0) + pow(distance_vec.y, 2.0);
      
         if (mag_sqr < searchLightRadius*searchLightRadius)
         {   
            fragColour = vec4(searchLightColour * (ambient + diffuse), 1.0);
         }
      }
   `;


   SOURCES.drawFragmentTexturedInstanced = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;
      in vec3 vNormal;

      uniform sampler2D tex;
      uniform vec3 lightDir;

      uniform vec3 searchLightPos;
      uniform float searchLightRadius;
      uniform vec3 searchLightColour;

      out vec4 fragColour;
      void main(void) 
      {  
         float ambient = 0.5;

         vec3 colour = texture(tex, vTexCoords).rgb;

         vec2 distance_vec = vPos.xz - searchLightPos.xz;
         float mag_sqr = pow(distance_vec.x, 2.0) + pow(distance_vec.y, 2.0);

         float searchLightRadiusSq = searchLightRadius * searchLightRadius;
      
         if (mag_sqr < searchLightRadiusSq)
         {
            float ratio = 1.0 - mag_sqr / searchLightRadiusSq;
            ratio = max(ambient, ratio + ambient);
            colour = colour + searchLightColour * 0.2;

            fragColour = vec4(colour * ratio, 1.0);
            return;
         }

         fragColour = vec4(colour * ambient, 1.0);
      }
   `;

   SOURCES.drawVertexUI = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;

      uniform mat4 proj;

      out vec3 vPos;
      out vec2 vTexCoords;

      void main(void) 
      {
         vec4 mPos = vec4(position, 1.0);
         gl_Position = proj * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
      }
   `;

   SOURCES.drawFragmentUI = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;

      uniform vec3 colour;

      out vec4 fragColour;
      void main(void) 
      {  
         fragColour = vec4(colour, 1.0);
      }
   `;



   function GetShaderSource(name) {
         return SOURCES[name];
   }

   return {
      GetShaderSource: GetShaderSource,
   };

})();