var SoundManager = (function() {

   var AUDIO_FILES = {};

   function IsPlaying(id) {
      if (AUDIO_FILES[id] == undefined) return false;
      
      return !AUDIO_FILES[id].paused;
   }

   async function Load(filePath, id) {
      AUDIO_FILES[id] = new Audio(filePath);

      await new Promise( (resolve, reject) => {
         AUDIO_FILES[id].oncanplay = () => {
            resolve();
         };
      });
   }


   function Play(id, loop) {
      if (AUDIO_FILES[id] != undefined) {
         if (loop) {
            AUDIO_FILES[id].loop = loop;
         }
         
         AUDIO_FILES[id].play();
      }
   }


   function Pause(id) {
      if (AUDIO_FILES[id] != undefined) {
         AUDIO_FILES[id].pause();
      }
   }


   function Reset(id) {      
      if (AUDIO_FILES[id] != undefined) {
         Pause(id);
         AUDIO_FILES[id].currentTime = 0;
      }
   }


   function SetVolume(id, volume) {
      if (AUDIO_FILES[id] != undefined) {
         AUDIO_FILES[id].volume = volume;
      }
   }

   return {
      IsPlaying: IsPlaying,
      Load: Load,
      Play: Play,
      Pause: Pause,
      Reset: Reset,
      SetVolume: SetVolume
   }
})();