var Model = function() {

   var gl;

   var vao;
   var vbo;
   var numTriangles = 0;

   var position = {x: 0, y: 0, z: 0, pitch: 0, yaw: 0, roll: 0};
   var boundingBox = {minX: 0, maxX: 0, minY: 0, maxY: 0, minZ: 0, maxZ: 0};
   var scale = {x: 1, y: 1, z: 1};
   var modelMatrix = new Matrix4();
   var textureID;
   var hasTexture = false;


   function InitGL(webgl) {
      gl = webgl;

      vao = gl.createVertexArray();
      vbo = gl.createBuffer();

      var verts = [
         // Front Face
         -0.5, -0.5, 0.5,     0, 0,       0, 0, 1,
         0.5, -0.5, 0.5,      1, 0,       0, 0, 1,
         0.5, 0.5, 0.5,       1, 1,       0, 0, 1,

         0.5, 0.5, 0.5,       1, 1,       0, 0, 1,
         -0.5, 0.5, 0.5,      0, 1,       0, 0, 1,
         -0.5, -0.5, 0.5,     0, 0,       0, 0, 1,

         // Right Face
         0.5, -0.5, 0.5,      0, 1,       1, 0, 0,
         0.5, -0.5, -0.5,     0, 0,       1, 0, 0,
         0.5, 0.5, -0.5,      1, 0,       1, 0, 0,

         0.5, 0.5, -0.5,      1, 0,       1, 0, 0,
         0.5, 0.5, 0.5,       1, 1,       1, 0, 0,
         0.5, -0.5, 0.5,      0, 0,       1, 0, 0,

         // Back Face
         0.5, -0.5, -0.5,     0, 0,       0, 0, -1,
         -0.5, -0.5, -0.5,    0, 0,       0, 0, -1,
         -0.5, 0.5, -0.5,     1, 0,       0, 0, -1,

         -0.5, 0.5, -0.5,     0, 1,       0, 0, -1,
         0.5, 0.5, -0.5,      1, 1,       0, 0, -1,
         0.5, -0.5, -0.5,     1, 0,       0, 0, -1,

         // Left Face
         -0.5, -0.5, -0.5,    0, 0,       -1, 0, 0,
         -0.5, -0.5, 0.5,     0, 1,       -1, 0, 0,
         -0.5, 0.5, 0.5,      1, 1,       -1, 0, 0,

         -0.5, 0.5, 0.5,      1, 1,       -1, 0, 0,
         -0.5, 0.5, -0.5,     1, 0,       -1, 0, 0,
         -0.5, -0.5, -0.5,    0, 0,       -1, 0, 0,

         // Top Face
         -0.5, 0.5, 0.5,      0, 1,       0, 1, 0,
         0.5, 0.5, 0.5,       1, 1,       0, 1, 0,
         0.5, 0.5, -0.5,      1, 0,       0, 1, 0,

         0.5, 0.5, -0.5,      1, 0,       0, 1, 0,
         -0.5, 0.5, -0.5,     0, 0,       0, 1, 0,
         -0.5, 0.5, 0.5,      0, 1,       0, 1, 0,

         // Bot Face
         -0.5, -0.5, -0.5,    0, 0,       0, -1, 0,
         0.5, -0.5, -0.5,     1, 0,       0, -1, 0,
         0.5, -0.5, 0.5,      1, 1,       0, -1, 0,

         0.5, -0.5, 0.5,      1, 1,       0, -1, 0,
         -0.5, -0.5, 0.5,     0, 1,       0, -1, 0,
         -0.5, -0.5, -0.5,    0, 0,       0, -1, 0
      ];

      SetVertexData(verts);
   }

   
   function Draw(shader) {
      gl.bindVertexArray(vao);

      if (hasTexture) {
         gl.activeTexture(gl.TEXTURE0);
         gl.bindTexture(gl.TEXTURE_2D, textureID);
         shader.Uniform1i("tex", 0);
      }

      shader.UniformMatrix4fv("model", modelMatrix);

      gl.drawArrays(gl.TRIANGLES, 0, numTriangles);

      gl.bindVertexArray(null);
   }


   function GetBoundingBox() {
      return boundingBox;
   }


   function SetPosition(x, y, z, pitch, yaw, roll) {
      position.x = x;
      position.y = y;
      position.z = z;
      position.pitch = pitch;
      position.yaw = yaw;
      position.roll = roll;

      var rotate = Matrix4.Rotate(position.pitch, position.yaw, position.roll);
      var translate = Matrix4.Translate(position.x, position.y, position.z);

      modelMatrix = Matrix4.Multiply(
         Matrix4.Scale(scale.x, scale.y, scale.z), 
         Matrix4.Multiply(rotate, translate));
   }


   function SetScale(x, y, z) {
      scale.x = x;
      scale.y = y;
      scale.z = z;

      var rotate = Matrix4.Rotate(position.pitch, position.yaw, position.roll);
      var translate = Matrix4.Translate(position.x, position.y, position.z);

      modelMatrix = Matrix4.Multiply(
         Matrix4.Scale(scale.x, scale.y, scale.z), 
         Matrix4.Multiply(rotate, translate));
   }


   function SetTextureID(id) {
      textureID = id;
      hasTexture = true;
   }


   function SetVertexData(vertexData) {
      for (var i = 0; i < vertexData.length; i+=8) {
         boundingBox.minX = Math.min(vertexData[i + 0], boundingBox.minX);
         boundingBox.minY = Math.min(vertexData[i + 1], boundingBox.minX);
         boundingBox.minZ = Math.min(vertexData[i + 2], boundingBox.minX);

         boundingBox.maxX = Math.max(vertexData[i + 0], boundingBox.maxX);
         boundingBox.maxY = Math.max(vertexData[i + 1], boundingBox.maxX);
         boundingBox.maxZ = Math.max(vertexData[i + 2], boundingBox.maxX);
      }

      numTriangles = Math.floor(vertexData.length / 8);

      gl.bindVertexArray(vao);
      
      gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexData), gl.STATIC_DRAW);
      gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 32, 0);
      gl.enableVertexAttribArray(0);
      gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 32, 12);
      gl.enableVertexAttribArray(1);
      gl.vertexAttribPointer(2, 3, gl.FLOAT, false, 32, 20);
      gl.enableVertexAttribArray(2);

      gl.bindVertexArray(null);
   }


   return {
      InitGL: InitGL,
      Draw: Draw,
      GetBoundingBox: GetBoundingBox,
      SetPosition: SetPosition,
      SetScale: SetScale,
      SetTextureID: SetTextureID,
      SetVertexData: SetVertexData,
   }
};