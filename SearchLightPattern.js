var SearchLightPattern = (function() {

   var PATTERN_SIZE = 0;
   var PatternNodes = [{x: 0, y: 0, z: 0}];
   var RESET_NEEDED = true;

   var targetNode = 0;

   function generate_spiral_pattern() {

      var radius = 32;

      var pattern_size = 300;
      var new_pattern = [];

      var i = 0;
      var step = 0;
      var num_revolutions = 3;
      var step_incr = (2 * num_revolutions *  Math.PI)/pattern_size;

      while (i < pattern_size && radius > 1){
         var new_pos = {x: 0, y: 0, z: 0};

         step += step_incr;

         new_pos.x = 32 + Math.cos(step) * radius;
         new_pos.z = 32 + Math.sin(step) * radius;

         new_pattern[i] = new_pos;

         radius -= 0.1;
         i++;
      }

      return new_pattern;
   }

   function generateLineSweepPattern() {
      var pattern = [];

      for (var i = 0; i < 4; i++) {
            var x = i * 16;
            var z = i % 2 == 0 ? 64 : 0;

            pattern[pattern.length] = {x: x, z: z};
      }

      return pattern;
   }

   function getNextSearchLight(searchLight, dt) {

      var current_position = {x: searchLight.x, z: searchLight.z};

      var path_dir = {
         x: PatternNodes[targetNode].x - current_position.x,
         z: PatternNodes[targetNode].z - current_position.z,
      };

      var mag_sqr = path_dir.x * path_dir.x + path_dir.z * path_dir.z;

      if (mag_sqr < 1 || RESET_NEEDED) {

         if (targetNode + 1 == PATTERN_SIZE || RESET_NEEDED) {

            targetNode = 0;

            var chance = Math.random();

            if (chance < 0.5) PatternNodes = generateLineSweepPattern();
            else PatternNodes = generate_spiral_pattern();

            PATTERN_SIZE = PatternNodes.length;

            RESET_NEEDED = false;
         }
         else targetNode++;
      }

      if (mag_sqr == 0) return searchLight;
      
      var invMag = 1 / Math.sqrt(mag_sqr);
      
      searchLight.x += searchLight.speed * path_dir.x * invMag * dt; 
      searchLight.z += searchLight.speed * path_dir.z * invMag * dt;
      searchLight.speed += 0.25 * dt;
      searchLight.radius += 0.25 * dt;

      return searchLight;
   }


   function Reset() {
      RESET_NEEDED = true;
   }

   return {
      getNextSearchLight: getNextSearchLight,
      Reset: Reset,
   }
})();


if (typeof exports !== 'undefined') {
   exports.SearchLightPattern = SearchLightPattern;
}